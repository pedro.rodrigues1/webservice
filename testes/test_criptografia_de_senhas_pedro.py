import unittest
from criptografia_de_senhas_pedro import hash_sha256, hash_md5


class TestHash (unittest.TestCase):

    def test1_pwd_igual(self):
        print("MD5 pwd iguais")
        pwd = "Abacaxi"
        pwd2 = "Abacaxi"
        teste1 = hash_md5(pwd)
        teste2 = hash_md5(pwd2)
        self.assertEqual(teste1, teste2)

    def test2_pwd_diferente(self):
        print("MD5 pwd diferentes")
        pwd = "Abacaxi"
        pwd2 = "Ab@c@x1"
        teste1 = hash_md5(pwd)
        teste2 = hash_md5(pwd2)
        self.assertNotEqual(teste1, teste2)

    def test3_pwd_vazio(self):
        print("MD5 pwd vazio")
        pwd = " "
        teste1 = hash_md5(pwd)
        self.assertEqual(teste1, "7215ee9c7d9dc229d2921a40e899ec5f")

    def test4_pwd_igual_sha256(self) :
        print ( "sha256 pwd iguais")
        pwd = "Cachorro"
        pwd2 = "Cachorro"
        teste1 = hash_sha256( pwd )
        teste2 = hash_sha256( pwd2 )
        self.assertEqual ( teste1, teste2 )
    
    
    def test5_pwd_diferente_sha256(self) :
        print ( "sha256 pwd diferentes")
        pwd = "Cachorro"
        pwd2 = "C@ch0rr0"
        teste1 = hash_sha256 ( pwd )
        teste2 = hash_sha256 ( pwd2 )
        self.assertNotEqual ( teste1, teste2 )


    def test6_pwd_vazio_sha256(self) :
        print ("sha256 pwd vazio")
        pwd = " "
        teste1 = hash_sha256 ( pwd )
        self.assertEqual ( teste1, "36a9e7f1c95b82ffb99743e0c5c4ce95d83c9a430aac59f84ef3cbfab6145068" )
    



if __name__ == '__main__':
    unittest.main()
