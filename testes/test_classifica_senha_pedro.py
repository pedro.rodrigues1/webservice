import unittest
import classificador_senha_pedro


class TestClassificaSenha (unittest.TestCase):

    def test_senha0_fraca(self):
        print("Testando senha: vazia - ", end="")
        pwd = ""
        teste = classificador_senha_pedro.classificador_de_senhas(pwd)
        self.assertEqual(teste, 0)

    def test_senha1_fraca(self):
        print("Testando senha: Pedro - ", end="")
        pwd = "Pedro"
        teste = classificador_senha_pedro.classificador_de_senhas(pwd)
        self.assertEqual(teste, 0)

    def test_senha2_fraca2(self):
        print("Testando senha: C@io - ", end="")
        pwd = "C@io"
        teste = classificador_senha_pedro.classificador_de_senhas(pwd)
        self.assertEqual(teste, 0)

    def test_senha3_media(self):
        print("Testando senha: Pedro1 - ", end="")
        pwd = "Pedro1"
        teste = classificador_senha_pedro.classificador_de_senhas(pwd)
        self.assertEqual(teste, 1)

    def test_senha4_media2(self):
        print("Testando senha: Pedro1Ralves - ", end="")
        pwd = "Pedro1Ralves"
        teste = classificador_senha_pedro.classificador_de_senhas(pwd)
        self.assertEqual(teste, 1)

    def test_senha5_forte(self):
        print("Testando senha: P&dr01R@lv&s - ", end="")
        pwd = "P&dr01R@lv&s"
        teste = classificador_senha_pedro.classificador_de_senhas(pwd)
        self.assertEqual(teste, 2)


if __name__ == '__main__':
    unittest.main()

