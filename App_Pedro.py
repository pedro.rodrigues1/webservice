from aiohttp import web
from servicos.retorna_romano_pedro import convercao_romana
from servicos.validacpf_pedro import valida_cpf
from servicos.conta_zeros_pedro import conta_zeros
from servicos.gera_senha_pedro import gera_senha
from servicos.classificador_senha_pedro import classificador_de_senhas
from servicos.criptografia_de_senhas_pedro import hash_md5
from paginas import pagina_romano, pagina_index, pagina_criptografia_senha, pagina_conta_zero , pagina_valida_cpf


async def index(request):
    return web.Response(text=pagina_index(), content_type='text/html')


async def converter_romano(request):
    numero = int(request.match_info['numero'])
    romano = str(convercao_romana(numero))
    
    return web.Response(text= pagina_romano(romano), content_type='text/html')


async def verifica_cpf(request):
    cpf = request.match_info['cpf']
    validacao = valida_cpf(cpf)
    if validacao:
        validacao="<font color=green> válido! </font>"
    else:
        validacao="<font color=red> inválido! </font>"
    return web.Response(text=pagina_valida_cpf(cpf, validacao) , content_type='text/html')


async def dist_zeros(request):
    string = request.match_info['string']
    zeros = conta_zeros(string)
    return web.Response(text=pagina_conta_zero(string, zeros) , content_type='text/html')


async def gerador_senha(request):
    senha_gerada = gera_senha()
    classificacao = classificador_de_senhas(senha_gerada)
    senha_hash = hash_md5(senha_gerada)
    if classificacao == 2:
        classificacao = "<font color=green>2 - Senha Forte!</font>"
    elif classificacao == 1:
        classificacao = "<font color=yellow> 1 - Senha Média! </font>"
    else:
        classificacao = "0 - Senha Fraca!"

    return web.Response(text=pagina_criptografia_senha(senha_gerada, classificacao, senha_hash) , content_type='text/html')




def setup_routes(app):  # definindo as rotas / get
    app.router.add_get('/', index)
    app.router.add_get('/romano/{numero}', converter_romano)
    app.router.add_get('/valida_cpf/{cpf}', verifica_cpf)
    app.router.add_get('/dist_zeros/{string}', dist_zeros)
    app.router.add_get('/gera_senha/', gerador_senha)
    


if __name__ == '__main__':

    app = web . Application()  # criando um app
    setup_routes(app)  # definindo a rota
    web . run_app(app, port=7854)  # especificando a porta
